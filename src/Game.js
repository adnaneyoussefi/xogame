import React, {useState} from 'react';

const Game = () => {
    const tab = ["","","","","","","","",""];
    const [board, setBoard] = useState(["","","","","","","","",""])
    const [x, setX] = useState(true);
    const [xx, setXx] = useState(0);
    const [oo, setOo] = useState(0);
    const patterns = ["012", "036", "258", "048", "678", "345", "147", "246"];
    const verif = () => {
        for(let pattern of patterns) {
            if([...pattern].every(index => board[index] === 'X')) {
                setBoard(tab);
                alert('X wins')
                setXx(xx + 1);
            }
            else if ([...pattern].every(index => board[index] === 'O')) {
                setBoard(tab);
                alert('O wins');
                setOo(oo + 1);
            }
            else if(board.every(b => b !== "")) {
                setBoard(tab);
            }
        }
    }

    const modifX = (i) => {
        if(board[i] === "") {
            setX(!x)
            board[i] = x ? "X" : "O"
            setBoard(board)
            verif()
        }
    }
    return (
        <div className="d-flex flex-column">
        <div className="resultat d-flex justify-content-center mb-5">X: {xx} O: {oo}</div>
        <div className="globSquare">
            {board.map((b, i) => {
                return (
                <div key={i} className="square" onClick={() => modifX(i)}>{b}</div>
                )
            })}
        </div>
        </div>
    )
}

export default Game
